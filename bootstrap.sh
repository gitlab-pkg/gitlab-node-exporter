#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# This is shell provisioner for Kitchen
set -eufo pipefail
IFS=$'\t\n'

dpkg -i /tmp/kitchen/data/gitlab-node-exporter_0.0.1.deb
