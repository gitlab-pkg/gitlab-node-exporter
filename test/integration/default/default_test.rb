# encoding: utf-8

# InSpec tests for gitlab-node-exporter package

control 'general-pkg-installed' do
  impact 1.0
  title 'General tests for gitlab-node-exporter package'
  desc '
    This control ensures that:
      * gitlab-node-exporter package is installed'

  describe package('gitlab-node-exporter') do
    it { should be_installed }
  end
end
