[![pipeline status](https://gitlab.com/gitlab-pkg/gitlab-node-exporter/badges/master/pipeline.svg)](https://gitlab.com/gitlab-pkg/gitlab-node-exporter/commits/master)
# gitlab-node-exporter

Build deb package for prometheus node exporter, test it and upload to aptly.
In a nutshell, this repo does:
 1. Build deb package. This is `make deb` task called from `.gitlab-ci.yml`.
    Since this particular package in only a repacked binary, for now it
    just downloads the binary, verifies signatures, and repacks it in deb.
 1. Test the deb. Ths is `make kitchen` responsibility. Apart from usual tasks
    of creating ephemeral keys and provisioning the instance on DO, it uses
    shell provisioner to install the deb from above step (see `bootstrap.sh`),
    and then runs InSpec integration tests against it. Throw whatever you
    need there to make sure installed deb meets all your criteria.
 1. Finally, if integration tests pass, and the change is merged to master,
    the deb package is uploaded to `ci-repo` of `aptly.gitlab.com`. For now,
    its simply published and ready to use. Later, we could throw in some
    pre-prod checks here, and maybe publishing to different repo.
    ```
# apt-cache policy gitlab-node-exporter
gitlab-node-exporter:
  Installed: (none)
  Candidate: 0.0.1
  Version table:
     0.0.1 500
        500 http://aptly.gitlab.com/ci xenial/main amd64 Packages

# grep aptly.gitlab.com /etc/apt/sources.list
deb http://aptly.gitlab.com/ci xenial main
```
    Just add the url into sources list and you're ready to go. Note: the
    packages are signed with our GPG key and are ready to use. In future,
    we need a key rotation procedures, but that's the scope of aptly itself.


TODO:
 - [ ] Replace `dpkg-deb` with something more robust (fpm?)
 - [ ] mirror to dev (?) thats _probably_ not needed since its not a cookbook,
       but still worth a thought
 - [ ] Write better integration tests for package
 - [ ] Write better docs
